from discord.ext import commands
from pprint import pprint
import yaml
import discord


class RachmaninoffBot(commands.Bot):

    def __init__(self, settings_file, mongodb_connection, command_prefix="!"):
        intents = discord.Intents.default()
        intents.typing = True
        intents.members = True
        # intents.message_content = True

        super().__init__(command_prefix, intents=intents)
        self.mongodb_connection = mongodb_connection
        with open(settings_file) as bot_settings:
            self.settings = (yaml.load(bot_settings, Loader=yaml.FullLoader))

    def add_cog(self, cog):
        if self.settings['verbose']:
            pprint("- Adding cog " + type(cog).__name__)

        cog.load_settings(self.settings)
        cog.mongodb_connection = self.mongodb_connection
        super().add_cog(cog)

    def run(self, token):
        pprint("Starting rachmaninoff bot.")
        super().run(token)

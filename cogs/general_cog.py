from .interface_cog import InterfaceCog
from pprint import pprint
from uptime import boottime
from speedtest import Speedtest
from discord.ext import commands
import discord
import requests
import random

class GeneralCog(InterfaceCog):
    @commands.Cog.listener()
    async def on_message(self, message):
        if message.author.name == 'Rachmaninoffs Bot':
            return

        self.log_action(message.author.name + ': ' + message.content)

    @commands.command(name='duck', aliases=['d'], help='Stupid fucking duck song.')
    async def duck(self, ctx):
        duck_embed = discord.Embed()
        duck_embed.add_field(name="Stupid Fucking Duck Song", value="https://www.youtube.com/watch?v=6iEyczP0qyQ")

        await ctx.send(embed=duck_embed)


    @commands.command(name='boottime', aliases=['bt'], help='Shows boottime of server where bot is hosted.')
    async def boottime(self, ctx):
        if not self.is_allowed(ctx):
            return

        boot_info = boottime()
        await ctx.send(boot_info.isoformat())

    @commands.command(name='speedtest', aliases=['st'], help='Runs speedtest on server where bot is hosted.')
    async def speedtest(self, ctx):
        if not self.is_allowed(ctx):
            return

        pprint('Running speedtest...')
        s = Speedtest()
        download = s.download()
        upload = s.upload()
        
        download_in_mb = download/(1000*1000)
        upload_in_mb = upload/(1000*1000)

        pprint('Finished running speedtest.')

        speedtest_results = "Download: {0}\nUpload: {1}"
        speedtest_results = speedtest_results.format(self.humansize(download), self.humansize(upload))

        speedtest_embed = discord.Embed()
        speedtest_embed.add_field(name="Speedtest Results", value=speedtest_results)

        await ctx.send(embed=speedtest_embed)

    @commands.command(name='catfact', aliases=['cf'], help='Random cat fact.')
    async def catfact(self, ctx):
        url = 'https://cat-fact.herokuapp.com/facts' 
        response = requests.get(url) 
        cat_facts = response.json()

        random_cat_fact = random.choice(cat_facts)
        catfact = random_cat_fact['text'];

        catfact_embed = discord.Embed()
        catfact_embed.add_field(name="Random Cat Fact", value=catfact)
        await ctx.send(embed=catfact_embed)

    @commands.command(name='numberfact', aliases=['nf'], help='Random number fact')
    async def numberfact(self, ctx):
        url = 'http://numbersapi.com/random/trivia'
        numberfact = requests.get(url)
 
        numberfact_embed = discord.Embed()
        numberfact_embed.add_field(name='Number Fact', value=numberfact.text)
        await ctx.send(embed=numberfact_embed)

    @commands.command(hidden=True)
    async def test(self, ctx):
        for command in self.bot.commands:
            pprint(command.name)

    def humansize(self, nbytes):
        suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
        i = 0
        while nbytes >= 1024 and i < len(suffixes)-1:
            nbytes /= 1024.
            i += 1
        f = ('%.2f' % nbytes).rstrip('0').rstrip('.')
        return '%s %s' % (f, suffixes[i])

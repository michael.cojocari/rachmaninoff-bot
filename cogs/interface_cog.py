from pprint import pprint
from discord.ext import commands
from pymongo import MongoClient

class InterfaceCog(commands.Cog):
    def __init__(self, bot):
        super().__init__()
        self.bot = bot
        self.verbose = False
        self.mongodb_connection = ''

    def is_allowed(self, ctx):
        return (ctx.author.id) in self.get_allowed_users()

    def log_action(self, message):
        if self.verbose:
            pprint("RB LOG --- " + message)
        # TODO: Add code to log to file if debug is flagged or something

    def load_settings(self, settings):
        self.verbose = settings['verbose']
        self.default_zipcode = settings['default_zipcode']

    def get_allowed_users(self):
        client = MongoClient(self.mongodb_connection)
        security_collection = client.rachmaninoff.security
        allowed_users = security_collection.find_one({'name': 'allowed_users'})
        return allowed_users['value']
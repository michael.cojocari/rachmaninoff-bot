from cogs.interface_cog import InterfaceCog
from discord.ext import commands
import discord
from pymongo import MongoClient


class SecurityCog(InterfaceCog):

    def __get_security_collection(self):
        client = MongoClient(self.mongodb_connection)
        return client.rachmaninoff.security

    @commands.command(name='special', help='Are you special?')
    async def special(self, ctx):
        if self.is_allowed(ctx):
            await ctx.send('Yes you are.')
        else:
            await ctx.send('No you are not.')

    @commands.command(name='list_allowed', aliases=['la'], help='Lists users that are allowed some stuff.')
    async def list_allowed(self, ctx):
        security_collection = self.__get_security_collection()
        allowed_results = ''

        current_channel = ctx.guild.get_channel(ctx.channel.id)
        allowed_users = security_collection.find_one({'name': 'allowed_users'})['value']

        for member in current_channel.members:
            if member.id in allowed_users:
                allowed_results = allowed_results + member.name + "\n"

        if allowed_results == '':
            allowed_results = 'No allowed users found.'

        allowed_embed = discord.Embed()
        allowed_embed.add_field(name='Allowed users', value=allowed_results)

        await ctx.send(embed=allowed_embed)

    @commands.command(name='add_allowed', aliases=['aa'], help='Adds new user to allowed users.')
    async def add_allowed(self, ctx, author):
        if not self.is_allowed(ctx):
            return

        security_collection = self.__get_security_collection()
        allowed_users = security_collection.find_one({'name': 'allowed_users'})['value']
        allowed_users.append(int(author[2:(len(author)-1)]))
        security_collection.update_one({'name': 'allowed_users'}, {'$set': {'value': allowed_users}})

        await self.list_allowed(ctx)

    @commands.command(name='delete_allowed', aliases=['da'], help='Deletes user from allowed list.', hidden=True)
    async def delete_allowed(self, ctx, author):
        if not self.is_allowed(ctx):
            return

        author_id = int(author[3:(len(author)-1)])
        security_collection = self.__get_security_collection()
        allowed_users = []
        for allowed_user in security_collection.find_one({'name': 'allowed_users'})['value']:
            if allowed_user != author_id:
                allowed_users.append(allowed_user)

        security_collection.update_one({'name': 'allowed_users'}, {'$set': {'value': allowed_users}})

        await self.list_allowed(ctx)



# bot.py
import os
from discord.ext import commands
from dotenv import load_dotenv
from rachmaninoff_bot import RachmaninoffBot
from cogs.general_cog import GeneralCog
from cogs.weather_cog import WeatherCog
from cogs.traffic_cog import TrafficCog
from cogs.stock_cog import StockCog
from cogs.security_cog import SecurityCog

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
MONGODB_CONNECTION = os.getenv('MONGODB_CONNECTION')
OPENWEATHERMAP_APIKEY = os.getenv('OPENWEATHERMAP_APIKEY')

bot = RachmaninoffBot(command_prefix='!', settings_file='rachmaninoff_settings.yml', mongodb_connection = MONGODB_CONNECTION)

bot.add_cog(TrafficCog(bot=bot))
bot.add_cog(GeneralCog(bot=bot))
bot.add_cog(WeatherCog(bot=bot, openweathermap_apikey=OPENWEATHERMAP_APIKEY))
bot.add_cog(StockCog(bot=bot))
bot.add_cog(SecurityCog(bot=bot))

bot.run(TOKEN)
